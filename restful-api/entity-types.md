## GroupType
| Name        | Type     | Description                                          |
|-------------|----------|------------------------------------------------------|
| id          | integer  |                                                      |
| name        | string   |                                                      |
| data        | json     |                                                      |
| groups      |array(int)|                                                      |

## Groups
| Name        | Type     | Description                                          |
|-------------|----------|------------------------------------------------------|
| id          | integer  |                                                      |
| name        | string   |                                                      |
| groupType   | integer  |                                                      |
| posts       |array(int)|                                                      |
| invite      | integer  |                                                      |
| data        | json     |                                                      |
| users       |array(int)|                                                      |
| moderators  |array(int)|                                                      |
| admins      |array(int)|                                                      |

## Invitelinks
| Name        | Type     | Description                                          |
|-------------|----------|------------------------------------------------------|
| id          | integer  |                                                      |
| togroup     | integer  |                                                      |
| link        | string   |                                                      |

## Post
| Name        | Type     | Description                                          |
|-------------|----------|------------------------------------------------------|
| id          | integer  |                                                      |
| title       | string   |                                                      |
| ingroup     | integer  |                                                      |
| logs        |array(int)|                                                      |
| tags        |array(str)|                                                      |
| content     | string   |                                                      |
| user        | integer  |                                                      |
| creationTime| date     |                                                      |
|expirationTime| date    |                                                      |

## Log
| Name        | Type     | Description                                          |
|-------------|----------|------------------------------------------------------|
| id          | integer  |                                                      |
| keyword     | string   |                                                      |
| message     | string   |                                                      |
| post        | integer  |                                                      |

## User
| Name        | Type     | Description                                          |
|-------------|----------|------------------------------------------------------|
| id          | integer  |                                                      |
| username    | string   |                                                      |
| roles       |array(role)|                                                     |
| password    | string   |                                                      |
| apiToken    | string   |                                                      |
| user        |array(int)| A list of group-id's the user is joined and has the user role |
| moderators  |array(int)| A list of group-id's the user is joined and has the moderator role |
| admin       |array(int)| A list of group-id's the user is joined and has the admin role |
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTc2MzA0NTkyNSwxNTcyMjAxNjI4LC00MD
E5MTIwODYsLTIwODg2ODMyNjgsMTg1Njg5MTA5OCwtMTM4MjM3
MTMzNywyMDk0MzQxMDI3LDQ5NzgxODgxMCw3MzA5OTgxMTZdfQ
==
-->
1. grouptypes
	1. [GET](#get-v01grouptypes)
	2. [/{ID} - GET](#get-v01grouptypesid)
	2. [POST](#post-v01grouptypes)
	3. [/{ID} - PUT](#put-v01grouptypesid)
	4. [/{ID} - DELETE](#delete-v01grouptypesid)
 2. groups
	 1. [GET](#get-v01groups)
	 2. [/{ID} - GET](#get-v01groupsid)
	 3. [POST](#post-v01groups)
	 4. [/{ID} - PUT](#put-v01groupsid)
	 5. [/{ID} - DELETE](#delete-v01groupsid)
 3. invitelinks
	 1. [GET](#get-v01invitelinks)
	 2. [/{ID} - GET](#get-v01invitelinksid)
	 3. [POST](#post-v01invitelinks)
	 4. [/{ID} - DELETE](#delete-v01invitelinksid)
 4. posts
	 1. [GET](#get-v01posts)
	 2. [/{ID} - GET](#get-v01postsid)
	 3. [POST](#post-v01posts)
	 4. [/{ID} - PUT](#put-v01postsid)
	 5. [/{ID} - DELETE](#delete-v01postsid)
 5. logs
	 1. [GET](#get-v01logs)
	 2. [/{ID} - GET](#get-v01logsid)
	 3. [/{ID}- DELETE](#delete-v01logsid)
 6. user
	 1. [GET](#get-v01user)
	 2. [/{ID} - GET](#get-v01userid)
	 3. [POST](#post-v01user)
	 4. [/{ID} - PUT](#put-v01userid)
	 5. [/{ID} - DELETE](#delete-v01userid)

# /v0.1/grouptypes
## GET /v0.1/grouptypes
#### Implementation Notes
This endpoint returns all grouptypes the user is permitted to access

#### Response Class (Status 200)
grouptypes Array
-   [Grouptype](Link)
-   [Example value](Link)

#### URI parameters
no parameters needed

#### Body parameters
no parameters needed


## GET /v0.1/grouptypes/{ID}
#### Implementation Notes
This endpoint returns the grouptype with the id {ID}, if the user is permitted to

#### Response Class (Status 200)
One grouptype
-   [Grouptype](Link)
-   [Example value](Link)

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the grouptype                                             | int        |

#### Body parameters
no parameters needed


## POST /v0.1/grouptypes
#### Implementation Notes
This endpoint returns grouptypes

#### Response Class (Status 200)
One grouptype
-   [Grouptype](Link)
-   [Example Value](Link)

#### URI parameters
no parameters needed

#### Body parameters

| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| name         | The name of the grouptype, displayed in the apps                | string     |
| groups       | A list of Group-ids that will get this grouptype  assigned      | array(int) |
| data         | predefines the data-attribute of groups assigned to this grouptype when created | array(int) |


## PUT /v0.1/grouptypes/{ID}
#### Implementation Notes
This endpoint updates a grouptype

#### Response Class (Status 200)
One grouptype
-   [Grouptype](Link)
-   [Example Value](Link)

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the grouptype                                             | int        |

#### Body parameters

| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| name         | The name of the grouptype, displayed in the apps                | string     |
| groups       | A list of Group-ids that will get this grouptype assigned       | array(int) |
| data         | predefines the data-attribute of groups assigned to this grouptype when created | array(int) |


## DELETE /v0.1/grouptypes/{ID}
#### Implementation Notes
This endpoint deletes a grouptype and the corresponding entities completely

#### Response Class (Status 200)
empty Object

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the grouptype                                             | int        |

#### Body parameters
no parameters needed


## groups
## GET /v0.1/groups
#### Implementation Notes
This endpoint returns all groups the user is allowed to access

#### Response Class (Status 200)
Group array
-   [Group](Link)
-   [Example Value](Link)

#### URI parameters
no parameters needed

#### Body parameters
no parameters needed


## GET /v0.1/groups/{ID}
#### Implementation Notes
This endpoint returns one group the user specified by the id

#### Response Class (Status 200)
One group
-   [Group](Link)
-   [Example Value](Link)

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the group                                                 | int        |

#### Body parameters
no parameters needed


## POST /v0.1/groups
#### Implementation Notes
This endpoint creates a group

#### Response Class (Status 200)
One group
-   [Group](Link)
-   [Example Value](Link)

#### URI parameters
no parameters needed

#### Body parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| name         | The name of the group, displayed in the apps                    | string     |
| groupType    | The grouptype id the group is related to                        | integer    |


## PUT /v0.1/groups/{ID}
#### Implementation Notes
This endpoint updates a group

#### Response Class (Status 200)
One group
-   [Group](Link)
-   [Example Value](Link)

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the group                                                 | int        |

#### Body parameters

| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| name         | The name of the group, displayed in the apps                    | string     |


## DELETE /v0.1/groups/{ID}
#### Implementation Notes
This endpoint deletes a group and all related entities

#### Response Class (Status 200)
empty Object

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the group, that should be deleted                         | int        |

#### Body parameters
no parameters needed



# /v0.1/invitelinks
## GET /v0.1/invitelinks
#### Implementation Notes
This endpoint returns all Invitelinks the user is permitted to access

#### Response Class (Status 200)
invitelinks array
-   [Invitelink](Link)
-   [Example Value](Link)

#### URI parameters
no parameters needed

#### Body parameters
no parameters needed


## GET /v0.1/invitelinks/{ID}
#### Implementation Notes
This endpoint returns one invitelink the user specified by the id

#### Response Class (Status 200)
One invitelink
-   [Invitelink](Link)
-   [Example Value](Link)

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the invitelink                                            | int        |

#### Body parameters
no parameters needed


## POST /v0.1/invitelinks
#### Implementation Notes
This endpoint creates an invitelink and assign it to the specified group

#### Response Class (Status 200)
One invitelink
-   [Invitelink](Link)
-   [Example Value](Link)

#### URI parameters
no parameters needed

#### Body parameters

| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| group        | The id of the group the link would be assign to                 | integer    |


## DELETE /v0.1/invitelinks/{ID}
#### Implementation Notes
This endpoint deletes an invitelink completely

#### Response Class (Status 200)
empty  Object

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the invitelink                                            | int        |

#### Body parameters
no parameters needed



# /v0.1/posts
## GET /v0.1/posts
#### Implementation Notes
This endpoint returns all posts the user is permitted to access

#### Response Class (Status 200)
post array
-   [Post](Link)
-   [Example Value](Link)

#### URI parameters
no parameters needed

#### Body parameters
no parameters needed


## GET /v0.1/posts/{ID}
#### Implementation Notes
This endpoint returns one post if the user is permitted to access the post specified by id

#### Response Class (Status 200)
post array
-   [Post](Link)
-   [Example Value](Link)

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the post                                                  | int        |

#### Body parameters
no parameters needed


## POST /v0.1/posts
#### Implementation Notes
This endpoint creates a post

#### Response Class (Status 200)
One post
-   [Post](Link)
-   [Example Value](Link)

#### URI parameters
no parameters needed

#### Body parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| title        | The title of the post showed in the apps                        | string     |
| ingroup      | Thr Group-id that will get this post assigned                   | integer    |
| tags         | Tags that specify the post content                             |array(string)|
| content      | The description of the post                                     | string     |
|expirationDate| The date and time when the post has expired                     | date       |


## PUT /v0.1/posts/{ID}
#### Implementation Notes
This endpoint updates a post

#### Response Class (Status 200)
One post
-   [Post](Link)
-   [Example Value](Link)

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the post                                                  | int        |

#### Body parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| title        | The title of the post showed in the apps                        | string     |
| ingroup      | Thr Group-id that will get this post assigned                   | integer    |
| tags         | Tags that specify the post content                             |array(string)|
| content      | The description of the post                                     | string     |
|expirationDate| The date and time when the post has expired                     | date       |


## DELETE /v0.1/posts/{ID}
#### Implementation Notes
This endpoint deletes a post

#### Response Class (Status 200)
One post
-   [Post](Link)
-   [Example Value](Link)

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the post                                                  | int        |

#### Body parameters
no parameters needed


# /v0.1/logs
## GET /v0.1/logs
#### Implementation Notes
This endpoint returns all postlogs the user is permitted to access

#### Response Class (Status 200)
log array
-   [PostLog](Link)
-   [Example Value](Link)

#### URI parameters
no parameter needed

#### Body parameters
no parameter needed


## GET /v0.1/logs/{ID}
#### Implementation Notes
This endpoint returns one log the user specified by id

#### Response Class (Status 200)
One log
-   [PostLog](Link)
-   [Example Value](Link)

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the log                                                   | int        |

#### Body parameters
no parameters needed


## DELETE /v0.1/logs/{ID}
#### Implementation Notes
This endpoint deletes a log completely

#### Response Class (Status 200)
One log
-   [PostLog](Link)
-   [Example Value](Link)

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the post                                                  | int        |

#### Body parameters
no parameters needed


# /v0.1/user
## GET /v0.1/user
#### Implementation Notes
This endpoint returns all user the user is permitted to access

#### Response Class (Status 200)
user array
-   [User](Link)
-   [Example Value](Link)

#### URI parameters
no parameters needed

#### Body parameters
no parameters needed


## GET /v0.1/user/{ID}
#### Implementation Notes
This endpoint returns one user specified by id

#### Response Class (Status 200)
One post
-   [Post](Link)
-   [Example Value](Link)

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the post                                                  | int        |

#### Body parameters
no parameters needed


## POST /v0.1/user
#### Implementation Notes
This endpoint creates a user

#### Response Class (Status 200)
One user
-   [User](Link)
-   [Example Value](Link)

#### URI parameters
no parameters needed

#### Body parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| username | The username of the user                                            | string     |
| password | The password of the user in clear text                              | string     |
| roles | The user roles the user will get                                       |array(roles)|
| groups | The group-id's the user will join direclty after account creation     | array(int) |


## PUT /v0.1/user/{ID}
#### Implementation Notes
This endpoint updates a user

#### Response Class (Status 200)
One user
-   [User](Link)
-   [Example Value](Link)

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the user                                                  | int        |

#### Body parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| username | The username of the user                                            | string     |
| password | The password of the user in clear text                              | string     |
| roles | The user roles the user will get                                       |array(roles)|


## DELETE /v0.1/user/{ID}
#### Implementation Notes
This endpoint deletes a user completely

#### Response Class (Status 200)
One user
-   [User](Link)
-   [Example Value](Link)

#### URI parameters
| Value        | Description                                                     | Data Type  |
|--------------|-----------------------------------------------------------------|------------|
| id           | id of the user                                                  | int        |

#### Body parameters
no parameters needed
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEwODAzNTYwMjMsMTAwNTE2MTM2NSwxMz
cyMjg3MjY3LDE3MTk5MTc3NjksMzExMTkwODQxLC0xNjM1MzYx
NTY1LC0xNTE0MTkzNzgzLC0xMTA2NTAxMTk4LDM5MjMxMDg5OC
wtMjY2OTgxNjYyLC05MjM3MTY5MjcsLTEzNDUxODI4MTQsMTg4
MDY4ODc0NCw2NDIwNDgwMjgsMTI0Njg1OTA0MSwtMzYwMTM0Nj
gsMTQwNDQ5NTA0MSwtOTM1NjQzMzg1LC0xODcyNzE2NDksLTEx
NDEzNzYyMjldfQ==
-->